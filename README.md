# Tutorials

Tutorials covering anything and everything that can be expressed in Markdown 
and should be shared throughout the DareData network.

We will write tutorials in a few different categories:

- **Sales**. How do we identify, engage, and find out if an organization can
  benefit from our services.
- **Project Management**. Once a project is sold, how we can organize ourselves
  and communicate with key stakeholders.
- **Software Engineering**. Everything we do has a component of software
  engineering. We should be the best engineers that we can.
- **Data Science**. Anything that is analysis, visualization, and ML.
- **Data Engineering** Anything having to do with ETLs.
- **Infrastructure**. Anything that includes architecture, provisioning, or
  CI/CD.


## How to write tutorials

Tutorials are implemented as Issues. To write a tutorial, you should do the
following:

- Open an issue. The title of the Issue is the name of the Tutorial.
- Assign the issue to yourself. The assignee of the issue is the person
  who commits to maintaining the issue.
- Write the tutorial in the body of the issue.
- Add the appropriate tags. At a minimum youo should add the `Tutorial` tag
  as well as the tag for one of the top-level directories.


## How to request tutorials

Go to othe [kanban board](https://gitlab.com/DareData-open-source/tutorials/-/boards) 
and add it into the backlog. If you're able to assign it to someone (or yourself)
feel free to do so!


## How to iterate on tutorials

This is essential as it will allow us to keep the quality of the tutorials
very high. Everyone must be allowed to interact and suggest improvements
to the issues and this should be done through the comments. Through the
comments you can suggest improvements **in markdown** so that the tutorial
maintainer can immediately copy-paste any suggestions they like into
the body of Tutorial.

If you are unable to maintain a tutorial any more, you must find a new person
to maintain it and change the assignee to the new person.


## How to find tutorials

All of the tutorials are written as GitLab issues. This is because they
can be tagged, searched, and discussed forum-style.

The best way to find them is to go to the [issues page](https://gitlab.com/DareData-open-source/tutorials/-/issues)
and filter what you are looking for. The most obvious filter
to include is the `Tutorial` filter since only issues with the `Tutorial`
tag are actually tutorials. Then you can filter by any of the keywords
mentioned above such as `Sales`, `Project Management`. We can also create
tags ad-hoc.

There are quick links for filtering that might be useful:

- [All Tutorials](https://gitlab.com/DareData-open-source/tutorials/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Tutorial)
- [Project Management](https://gitlab.com/DareData-open-source/tutorials/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Tutorial&label_name[]=Project%20Management)
- [Infrastructure](https://gitlab.com/DareData-open-source/tutorials/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Infrastructure&label_name[]=Tutorial)
- [Software Engineering](https://gitlab.com/DareData-open-source/tutorials/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Software%20Engineering&label_name[]=Tutorial)
- [Data Engineering](https://gitlab.com/DareData-open-source/tutorials/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Data%20Engineering&label_name[]=Tutorial)
- TODO add the rest as they are written


### Tutorial Directory

## Project Management

If you are new to the orgainzation, you should go through these tutorials
in this order.

1. [Client GDrive Organization](https://gitlab.com/DareData-open-source/tutorials/-/issues/13)
1. [Access control to GDrive folders using Google Groups](https://gitlab.com/DareData-open-source/tutorials/-/issues/16)
1. [Writing Sprint Documents](https://gitlab.com/DareData-open-source/tutorials/-/issues/14)
1. [Gitlab -> Slack notifications](https://gitlab.com/DareData-open-source/tutorials/-/issues/18)
    - Wasn't sure if this should be somewhere else but since all contractors 
      are technical, you can do all of this with a browser, and it's about 
      communication, it could be here.
1. [Time tracking with timely](https://gitlab.com/DareData-open-source/tutorials/-/issues/31)

## Software Engineering

1. [Python Development Environment](https://gitlab.com/DareData-open-source/tutorials/-/issues/34)
1. [Automated testing essential concepts](https://gitlab.com/DareData-open-source/tutorials/-/issues/23)
1. [Gitlab CI/CD pipelines](https://gitlab.com/DareData-open-source/tutorials/-/issues/30)

## Data Engineering

1. [SQL ETLs to build Data Marts and Warehouses](https://gitlab.com/DareData-open-source/tutorials/-/issues/26)
1. [Digdag basics](https://gitlab.com/DareData-open-source/tutorials/-/issues/28)
1. [Deploying and scheduling SQL ETLs with digdag](https://gitlab.com/DareData-open-source/tutorials/-/issues/27)
1. [Snowflake basics](https://gitlab.com/DareData-open-source/tutorials/-/issues/29)

## Infrastructure

1. [Terraform basics](https://gitlab.com/DareData-open-source/tutorials/-/issues/22)
1. [Ansible basics](https://gitlab.com/DareData-open-source/tutorials/-/issues/25)
1. [HTTPS on EC2 with LetsEncrypt on AWS](https://gitlab.com/DareData-open-source/tutorials/-/issues/24)
1. [Snowflake infrastructure](https://gitlab.com/DareData-open-source/tutorials/-/issues/32)

## DareData internal

1. [Invoicing DareData](https://gitlab.com/DareData-open-source/tutorials/-/issues/33)
2. [Open Activity on Finanças](https://gitlab.com/DareData-open-source/tutorials/-/issues/20)

## Data Science

TODO

## Sales

TODO


